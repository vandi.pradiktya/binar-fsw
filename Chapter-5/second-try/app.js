const express = require('express')
const app = express()
const md5 = require('md5');
const port = 3000


app.use(express.json());
app.use(express.static("public"));
app.use(express.static("./public/Chapter-3"));


const accountList = [];

app.get('/', (req, res) => {
  res.sendFile('./public/Chapter-3/index.html')
});

app.get('/data', (req,res) => {
    res.json(accountList)
})

app.post('/data', (req,res) => {
    const apiFill = req.body

    // cek username email dan password exist
    if (apiFill.user === undefined || apiFill.user === "") {
        return res.json("Please Fill Username")
    }

    if (apiFill.email === undefined || apiFill.email === "") {
        return res.json("Please Fill Email")
    }

    if (apiFill.password === undefined || apiFill.password === "") {
        return res.json("Please Fill Password")
    }

    //cek user dan email teregistrasi // still doesnt work
    const registeredUser = accountList.find((x) => {
        return (x.user === apiFill.user || x.email === apiFill.email);
    }) 
    
    if (registeredUser) {
        return res.json(" Username/email already exist")
    }

    const requiredField = {
        id : (accountList.length + 1),
        user : apiFill.user,
        email : apiFill.email,
        password : md5(apiFill.password)
    }

    accountList.push(requiredField);

    res.json ('Registration Success');
})

// Login
app.post("/login", (req,res) => {
    const {user, password} = req.body;
    const dataUser = accountList.find((x) => {
        return x.user === user && x.password === md5(password);
    });

    if (dataUser) {
        return res.json("login berhasil");
    } else {
        return res.json("username/password salah");
    }
});












app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})