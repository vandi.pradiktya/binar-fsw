const express = require('express');
const app = express();
const port = 3000;
const userRouter = require('./user/route');

app.use(express.json());
app.use(express.static("public"));
app.use(express.static("./public/Chapter-3"));
app.use('/', userRouter);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})