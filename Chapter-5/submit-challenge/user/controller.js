const userModel = require("./model");


class UserController {
    showHomepage = (req, res) => {
        res.sendFile('./public/Chapter-3/index.html')
      }

      // all user
    getAllAccount = async (req,res) => {
        const allUser = await userModel.getAccountList();
        res.json(allUser);
    }

    //single user
    singleUser = async (req,res) => {
        const { idUser } = req.params;
        try {
            const users = await userModel.singleUser(idUser);

            if (users) {
                return res.json(users);
            }
            else
            return res.json (`user tidak ditemukan`);
        }
        catch (error) {
            return res.json('id tidak ditemukan');
        }
    }

    //update single user
    updateOneUser = async (req,res) => {
        const { idUser } = req.params;
        // console.log(idUser);
        const { fullname, address } = req.body;
        const updateUser = await userModel.updateOneUser(idUser, fullname, address);

        return res.json(updateUser);
       
    }


    registerUser = async (req,res) => {
        const apiFill = req.body
    
        // cek username email dan password exist
        if (apiFill.user === undefined || apiFill.user === "") {
            return res.json("Please Fill Username")
        }
    
        if (apiFill.email === undefined || apiFill.email === "") {
            return res.json("Please Fill Email")
        }
    
        if (apiFill.password === undefined || apiFill.password === "") {
            return res.json("Please Fill Password")
        }
    
        //cek user dan email teregistrasi 
        const registeredUser = await userModel.isUserRegistered(apiFill);
        
        if (registeredUser) {
            return res.json(" Username/email already exist")
        }
    
        // record new account
        userModel.recordNewData(apiFill);
    
        res.json ('Registration Success');
    }

    loginUser = async (req,res) => {
        const {user, password} = req.body;
        const dataLogin = await userModel.verifyLogin(user, password);
    
        if (dataLogin) {
            return res.json("login berhasil");
        } else {
            return res.json("username/password salah");
        }
    }
}

module.exports = new UserController();