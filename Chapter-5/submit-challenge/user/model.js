const accountList = [];
const md5 = require('md5');
const db = require('../db/models');
const { Op, or } = require ('sequelize');

class UserModel {
    getAccountList = async () => {
        // return accountList;
        return await db.User.findAll({include:[db.UserBiodata]});

    };

    //get single user
    singleUser = async (idUser) => {
        return await db.User.findOne({where :{id:idUser}})
    }

    //update single user
    updateOneUser = async (idUser, fullname, address) => {
        return await db.UserBiodata.create(
            { fullname : fullname,
            address : address,
            user_id: idUser},
        );
    };

    isUserRegistered = async (apiFill) => {
        const registeredUser = await db.User.findOne ({
            where: {[Op.or]:
            [{user : apiFill.user},
                {email : apiFill.email}]
            }})
        // const registeredUser = accountList.find((x) => {
        //     console.log (apiFill);
        //     return (x.user === apiFill.user || x.email === apiFill.email);
        // }); 

        if (registeredUser) {
            return true
        } else {
            return false
        };
    };

    // record new account
    recordNewData = (apiFill) => {
        const requiredField = {
            user : apiFill.user,
            email : apiFill.email,
            password : md5(apiFill.password)
        }
    
        // accountList.push(requiredField);
        db.User.create(requiredField);

    }

    //verify login
    verifyLogin = async (user, password) => {
        // const dataUser = accountList.find((x) => {
        //     return x.user === user && x.password === md5(password);
        // });

        const dataUser = await db.User.findOne ({where : {user:user, password:md5(password)}})

        return dataUser;
    }
};

module.exports = new UserModel ();