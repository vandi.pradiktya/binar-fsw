const express = require ('express');
const userRouter = express.Router();
// const userModel = require("./model");
const UserController = require('./controller');

//Show Homepage
userRouter.get('/', UserController.showHomepage);

//Show account list
userRouter.get('/data', UserController.getAllAccount);

//show single user id
userRouter.get("/data/:idUser", UserController.singleUser);

//update singleuser
userRouter.post("/data/:idUser", UserController.updateOneUser);


//Registration
userRouter.post('/data', UserController.registerUser);

// Login
userRouter.post("/login", UserController.loginUser);

module.exports = userRouter;